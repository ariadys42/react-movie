import React, { Component } from 'react';
import { POPULAR_URL, SEARCH_URL, IMAGE_BASE_URL, POSTER_SIZE } from '../config/config';

import Grid from './elements/Grid';
import MovieThumb from './elements/MovieThumb';
import Spinner from './elements/Spinner';
import SearchBar from './elements/SearchBar';
import LoadMoreBtn from './elements/LoadMoreBtn';

import Logo from '../images/logo.png';
import NoImage from '../images/no_image.jpg';
import { StyledHeader, StyledLogo } from './styles/StyledHeader';

class Home extends Component {
    state = {
        movies: [],
        searchTerm: '',
        loading: true,
        error: false,
    }

    fetchMovies = async endpoint => {
        this.setState({ loading: true, error: false });
        const isLoadMore = endpoint.search('page');

        try {
            const result = await (await fetch(endpoint)).json();
            this.setState(prev => ({
              ...prev,
              movies:
                isLoadMore !== -1
                  ? [...prev.movies, ...result.results]
                  : [...result.results],
              currentPage: result.page,
              totalPages: result.total_pages,
              loading: false,
            }));
        } catch (error) {
            this.setState({ error: true });
            console.log(error);
        }
    }

    componentDidMount() {
        this.fetchMovies(POPULAR_URL);
    }

    searchMovies = search => {
        const endpoint = search ? SEARCH_URL + search : POPULAR_URL;

        this.setState({ searchTerm: search });
        this.fetchMovies(endpoint);
    }

    loadMoreMovies = () => {
        const { searchTerm, currentPage } = this.state;
        const searchEndPoint = `${SEARCH_URL}${searchTerm}&page=${currentPage+1}`;
        const popularEndPoint = `${POPULAR_URL}&page=${currentPage+1}`;

        const endpoint = searchTerm ? searchEndPoint : popularEndPoint;
        this.fetchMovies(endpoint);
    }

    render() {
        const { searchTerm, movies, currentPage, totalPages, loading, error } = this.state;
        if(error) return <div>Something Wrong</div>;
        return (
            <>
            <StyledHeader>
                <div className="header-content">
                    <StyledLogo src={Logo} alt='logo' />
                    <SearchBar callback={this.searchMovies} />
                </div>
            </StyledHeader>
                <Grid header={searchTerm && movies[0] ? 'Search Result' : !movies[0] ? 'Movie not Found' : 'Popular Movies'}>
                    {movies.map(movie => (
                        <MovieThumb
                            key={movie.id}
                            clickable
                            image={movie.poster_path ? `${IMAGE_BASE_URL}${POSTER_SIZE}${movie.poster_path}`: NoImage}
                            movieId={movie.id}
                            movieName={movie.original_title}
                        />
                    ))}
                </Grid>
                {loading && <Spinner />}
                {currentPage < totalPages && !loading && (
                    <LoadMoreBtn text="Load More" callback={this.loadMoreMovies} />
                )}
            </>
        )
    }
}

export default Home;