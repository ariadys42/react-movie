import React from 'react';
import PropTypes from 'prop-types';

import { IMAGE_BASE_URL, POSTER_SIZE } from '../../config/config';

import MovieThumb from './MovieThumb';

import NoImage from '../../images/no_image.jpg';
import { StyledMovieInfo } from '../styles/StyledMovieInfo';

const MovieInfo = ({ movie }) => {
  const date = new Date(movie.release_date); 
  const elapsed = date.getFullYear();

  return(
  <StyledMovieInfo>
    <div className="movieinfo-content">
      <div className="movieinfo-thumb">
        <MovieThumb
          image={
            movie.poster_path
              ? `${IMAGE_BASE_URL}${POSTER_SIZE}${movie.poster_path}`
              : NoImage
          }
          clickable={false}
          alt="moviethumb"
        />
      </div>
      <div className="movieinfo-text">
        <h1>{movie.title}</h1>
        <p>{movie.overview}</p>
        <div className="rating-director">
          <div>
            <h3>IMDB RATING</h3>
            <div className="score">{movie.vote_average}</div>
          </div>
          <div className="director">
            <h3>DIRECTORS</h3>
            {movie.directors.map(element => (
              <p key={element.credit_id}>{element.name}</p>
            ))}
          </div>
        </div>
        <div className="listInfo">
            <p>Genres: {movie.genres.map(element => (
              element.name+', '
            ))}</p>
            <p>Year of Release: {elapsed}</p>
            <p>Status: {movie.status}</p>
            <p>Production Countries: {movie.production_countries.map(element => (
              element.name+', '
            ))}</p>
            <p>Production Companies: {movie.production_companies.map(element => (
              element.name+', '
            ))}</p>
            <p>Source: {movie.homepage}</p>
        </div>
      </div>
    </div>
  </StyledMovieInfo>
)};

MovieInfo.propTypes = {
  movie: PropTypes.object,
  directors: PropTypes.array,
}

export default MovieInfo;
