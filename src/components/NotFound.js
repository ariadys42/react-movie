import React, { Component } from 'react';

class NotFound extends Component {
    render() {
        return(
            <div>Whoooops ... nothing found here...</div>
        )
    }
};

export default NotFound;