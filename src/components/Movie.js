import React, { Component } from 'react';
import { API_KEY, API_URL } from '../config/config';

import Navigation from './elements/Navigation';
import MovieInfo from './elements/MovieInfo';
import Spinner from './elements/Spinner';

class Movie extends Component {
  state = { loading: true };

  fetchData = async () => {
    const { movieId } = this.props;
    this.setState({ loading: true, error: false });

    try {
      const endpoint = `${API_URL}movie/${movieId}?api_key=${API_KEY}`;
      const result = await (await fetch(endpoint)).json();

      const creditsEndpoint = `${API_URL}movie/${movieId}/credits?api_key=${API_KEY}`;
      const creditsResult = await (await fetch(creditsEndpoint)).json();
      const directors = creditsResult.crew.filter(
        member => member.job === 'Director'
      );

      this.setState({
        ...result,
        directors,
        loading: false,
      })

    } catch (error) {
      this.setState({ error: true });
      console.log(error);
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  render() {
    const { original_title: originalTitle, loading, error } = this.state;

    if (error) return <div>Something went wrong ...</div>;
    if (loading) return <Spinner />;

    return (
    <>
      <Navigation movie={originalTitle} />
      <MovieInfo movie={this.state} />
    </>
    )
  }

  
};

export default Movie;