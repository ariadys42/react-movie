const API_URL = 'https://api.themoviedb.org/3/';
const API_KEY = 'd3432bde063cb19994df47c3af6ef0be';
const IMAGE_BASE_URL = 'http://image.tmdb.org/t/p/';

const SEARCH_URL = `${API_URL}search/movie?api_key=${API_KEY}&query=`;
const POPULAR_URL = `${API_URL}movie/popular?api_key=${API_KEY}`;

const POSTER_SIZE = 'w500';

export { SEARCH_URL,  POPULAR_URL, API_URL, API_KEY, IMAGE_BASE_URL, POSTER_SIZE };
