import React from 'react';
import { Router } from '@reach/router';

import Home from './components/Home';
import Movie from './components/Movie';
import NotFound from './components/NotFound';

import { GlobalStyle } from './components/styles/GlobalStyle';

const App = () => (
    <>
        <Router>
            <Home path="/" />
            <Movie path="/:movieId" />
            <NotFound default />      
        </Router>
        <GlobalStyle />
    </>
)

export default App;
